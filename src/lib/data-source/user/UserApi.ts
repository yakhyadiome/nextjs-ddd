import type { UserProps } from "@/lib/domains/user/type";

const dummyUsers: UserProps[] = [
  {
    id: "1",
    name: "artist coder",
  },
];

function getUsers(name: string): Promise<UserProps | null> {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(dummyUsers.find((user) => user.name === name) ?? null);
    }, 2000);
  });
}

export const UserRepository = {
  getUsers,
};
