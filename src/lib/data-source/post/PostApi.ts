import { NewPost, PostProps } from "@/lib/domains/post/type";
import { faker } from "@faker-js/faker";

const dummyPosts: PostProps[] = [
  {
    id: "1",
    title: "post 1",
    author: "eric",
    content: faker.lorem.paragraph(Math.round(Math.random() * 50)),
  },
  {
    id: "2",
    title: "post 2",
    author: "eric",
    content: faker.lorem.paragraph(Math.round(Math.random() * 50)),
  },
  {
    id: "3",
    title: "post 3",
    author: "john",
    content: faker.lorem.paragraph(Math.round(Math.random() * 50)),
  },
  {
    id: "4",
    title: "post 4",
    author: "john",
    content: faker.lorem.paragraph(Math.round(Math.random() * 50)),
  },
];

function list(): Promise<PostProps[]> {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(dummyPosts);
    }, 2000);
  });
}

function getPost(id: string): Promise<PostProps | null> {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(dummyPosts.find((post) => post.id === id) ?? null);
    }, 2000);
  });
}

function createPost(post: NewPost) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(dummyPosts.push({ ...post, id: "12" }));
    }, 10000);
  });
}

export const PostRepository = {
  list,
  getPost,
  createPost,
};
