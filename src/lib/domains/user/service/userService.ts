import { UserRepository } from "@/lib/data-source/user/UserApi";

class UserService {
  static getUserDetail(name: string) {
    return UserRepository.getUsers(name);
  }
}

export default UserService;
