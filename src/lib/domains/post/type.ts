export type PostProps = {
  id: string;
  title: string;
  author: string;
  content: string;
};

export type NewPost = {
  title: string;
  content: string;
  author: string;
};
