import Post from "../Post";
import { PostRepository } from "@/lib/data-source/post/PostApi";
import { NewPost } from "../type";

class PostService {
  static list() {
    return PostRepository.list().then((posts) => {
      return posts.map(
        (post) => new Post(post.id, post.title, post.author, post.content)
      );
    });
  }
  static getPost(id: string) {
    return PostRepository.getPost(id).then((post) =>
      post ? new Post(post.id, post.title, post.author, post.content) : null
    );
  }

  static createPost(post: NewPost) {
    return PostRepository.createPost(post);
  }
}

export default PostService;
