import type { PostProps } from "./type";

class Post {
  constructor(
    private readonly id: string,
    private readonly title: string,
    private readonly author: string,
    private readonly content: string
  ) {}

  static fromProperties(properties: PostProps) {
    return new Post(
      properties.id,
      properties.title,
      properties.author,
      properties.content
    );
  }

  get properties(): PostProps {
    return {
      id: this.id,
      title: this.title,
      author: this.author,
      content: this.content,
    };
  }
}

export default Post;
