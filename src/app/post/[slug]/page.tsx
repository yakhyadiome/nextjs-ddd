import PostService from "@/lib/domains/post/service/postService";

export default async function Post({ params }: { params: any }) {
  const post = await PostService.getPost(params.slug);

  if (!post) {
    return;
  }

  return (
    <div style={{ padding: 10 }}>
      <h2 style={{ textAlign: "center", padding: 10 }}>
        {post.properties.title}
      </h2>
      <p style={{ padding: "0 15px", marginBottom: 15 }}>
        {post.properties.content}
      </p>
      <h3 style={{ padding: 10 }}>Author: {post.properties.author}</h3>
    </div>
  );
}
