"use client";

import PostService from "@/lib/domains/post/service/postService";
import { useRouter } from "next/navigation";
import { useState } from "react";

export default function NewPost() {
  const router = useRouter();
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    setIsLoading(true);

    try {
      await PostService.createPost({ title, content, author: "me" });
      setIsLoading(false);
      setTitle("");
      setContent("");
      router.push("/");
    } catch (e) {
      console.log(e);
      setIsLoading(false);
    }
  };

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        margin: "0 auto",
        width: "90%",
      }}
    >
      <h1 style={{ textAlign: "center" }}>New Post:</h1>
      <div className="input-group">
        <span>Title</span>
        <input
          name="title"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          placeholder="post title"
        />
      </div>
      <div className="input-group">
        <span>Content</span>
        <textarea
          name="content"
          placeholder="content"
          value={content}
          onChange={(e) => setContent(e.target.value)}
        />
      </div>
      <button className="btn" disabled={isLoading} onClick={handleSubmit}>
        {isLoading ? "loading..." : "Create"}
      </button>
    </div>
  );
}
