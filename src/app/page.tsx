import styles from "./page.module.css";
import PostService from "@/lib/domains/post/service/postService";
import Link from "next/link";

export default async function Home() {
  const posts = await PostService.list();

  if (!posts) {
    return <div>loading...</div>;
  }

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-between",
        flexDirection: "column",
        width: "80%",
        margin: "0 auto",
      }}
    >
      <Link
        href="/post/new-post"
        className="btn"
        style={{ width: "140px", alignSelf: "end" }}
      >
        New Post
      </Link>
      {posts.map((post) => (
        <div key={post.properties.id} style={{ padding: 10 }}>
          <Link href={`/post/${post.properties.id}`}>
            <h2>{post.properties.title}</h2>
          </Link>
          <p style={{ padding: "15px 10px" }}>
            {post.properties.content?.slice(0, 50) + "..."}
          </p>
          <h3>Author: {post.properties.author}</h3>
        </div>
      ))}
    </div>
  );
}
